import React from 'react';
import './App.css';
import axios from 'axios';
import LoginForm from './components/LoginForm/index'
import Content from './components/Content/index'
import Loader from './components/Loader/index'

class App extends React.Component {
  constructor(props){
    super(props);
    this.state = {
      isAuth: false,
      tokenChecked: false
    };
  }

  loginSuccess = () => this.setState({isAuth: true, tokenChecked: true});

  verifyToken = () => {
    this.setState({isAuth: true})

    const token = localStorage.getItem('access');
    const body = JSON.stringify({ token })
    const headers = {
      'Content-Type': 'application/json'
    }
    axios.post('https://zonesmart.su/api/auth/jwt/verify/', body, {headers})
      .then(res => {
        console.log('token is okay');
        this.setState({tokenChecked: true})
      })
      .catch(err => {
        console.log('renew token')
        this.refreshToken();
      })
  }

  refreshToken = () => {
    const refresh = localStorage.getItem('refresh');
    const body = JSON.stringify({ refresh })
    const headers = {
      'Content-Type': 'application/json'
    }
    axios.post('https://zonesmart.su/api/auth/jwt/refresh/', body, {headers})
      .then(res => {
        const { access } = res.data;
        localStorage.setItem('access', access);
        console.log('now token is okay');
        this.setState({tokenChecked: true});
      })
      .catch(err => {
        console.log('token is not okay');
        this.setState({isAuth: false});
      })
  }

  componentWillMount(){
    localStorage.getItem('access') && this.verifyToken();
  }

  render(){
    const { isAuth, tokenChecked } = this.state;
    return (
      <div className='cont'>
        {
          isAuth 
          ?
            tokenChecked ?
              <Content />
            :
              <Loader />
          :
            <LoginForm loginSuccess={this.loginSuccess} />
        }
      </div>
    );
  }

}

export default App;
