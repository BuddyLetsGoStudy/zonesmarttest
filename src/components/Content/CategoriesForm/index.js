import React, { Component } from 'react'
import axios from 'axios';
import PropTypes from 'prop-types'
import Loader from '../../Loader/index'
import * as _ from 'lodash';
import './main.css';

export default class CategoriesForm extends Component {
    static propTypes = {
        channel: PropTypes.string,
        aspectsNeeded: PropTypes.func
    }

    constructor(props){
        super(props);
        this.state = {
            categories: [],
            level: 1
        };
    }

    componentDidUpdate(prevProps, prevState){
        if (prevProps.channel !== this.props.channel){
            this.setState({categories: []});
            this.fetchCategories(1, null);
        }
    }

    fetchCategories = (level, parent_id) => {
        const { channel } = this.props;
        const token = localStorage.getItem('access');
        const headers = {
          'Authorization': `JWT ${token}`
        }
        axios.get(`https://zonesmart.su/api/v1/ebay/category/?limit=1000&level=${level}&domain_id=${channel}${parent_id ? '&parent_id=' + parent_id : ''}`, {headers})
          .then(res => {
            const obj = {
                categories: res.data.results,
                level
            };
            this.setState(prevState => ({categories: [..._.filter([...prevState.categories], block => block.level < level), obj]}));


          })
          .catch(err => {
            console.log(err);
          })
    };

    changeCategory = (e) => {
        const id = e.target.value;
        const category = _.find(_.find(this.state.categories, {categories:[{id}]}).categories, {id});
        const { level, category_id, is_leaf } = category;
        if (is_leaf) {
            this.props.aspectsNeeded(id);
        } else {
            this.props.aspectsNeeded('');
            this.fetchCategories(level + 1, category_id);
        }
    };

    componentDidMount(){
        this.fetchCategories(1, null);
    }


    render() {
        const { categories } = this.state;
        if (!categories.length) {
            return <Loader />
        } else{
            return (
                <div className={'categories-cont'}>
                {
                    categories.map((categoriesOneLevel, i) => 
                        <select key={i} onChange={this.changeCategory} defaultValue={'0'} className={'categories-select'}>
                            <option disabled={true} value={'0'}>Select a category</option>
                            {
                                categoriesOneLevel.categories.map(category => 
                                    <option key={category.id} value={category.id}>{category.name}</option>
                                )
                            }
                        </select>
                    )
                }
                </div>
               
            )
        }
    }
}
