import React, { Component } from 'react'
import axios from 'axios';
import Loader from '../../Loader/index'
import PropTypes from 'prop-types';
import './main.css';


export default class ChannelsForm extends Component {
    static propTypes = {
        channel: PropTypes.string,
        changeChannel: PropTypes.func
    }

    constructor(props){
        super(props);
        this.state = {
          channels: [],
        };
    }

    fetchChannels = () => {
        const token = localStorage.getItem('access');
        const headers = {
          'Authorization': `JWT ${token}`
        }
        
        axios.get('https://zonesmart.su/api/v1/marketplace/channel/', {headers})
          .then(res => {
            this.setState({channels: res.data.results});
          })
          .catch(err => {
            console.log(err);
          })
    };

    componentDidMount(){
        this.fetchChannels();   
    }

    render() {
        const { channels } = this.state;
        if (!channels.length) {
            return <Loader />
        } else{
            return (
                <select onChange={this.props.changeChannel} value={this.props.channel} className={'channels-select'}>
                    <option disabled={true} value={'0'}>Select a channel</option>
                    {
                        channels.map(channel => 
                            <option key={channel.id} value={channel.domain}>{channel.name}</option>
                        )
                    }
                </select>
            )
        }
       
    }
}
