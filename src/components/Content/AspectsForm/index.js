import React, { Component } from 'react'
import axios from 'axios';
import PropTypes from 'prop-types'
import Loader from '../../Loader/index'
import './main.css';

export default class AspectsForm extends Component {
    static propTypes = {
        category: PropTypes.string
    }

    constructor(props){
        super(props);
        this.state = {
          aspects: [],
        };
    }

    fetchAspects = () => {
        const token = localStorage.getItem('access');
        const headers = {
          'Authorization': `JWT ${token}`
        }
        
        axios.get(`https://zonesmart.su/api/v1/ebay/category/${this.props.category}/aspect/`, {headers})
          .then(res => {
            this.setState({aspects: res.data});
          })
          .catch(err => {
            console.log(err);
          })
    };

    componentDidUpdate(prevProps, prevState){
        if (prevProps.category !== this.props.category){
            this.setState({aspects: []});
            this.fetchAspects();
        }
    }

    componentDidMount(){
        this.fetchAspects();
    }

    render() {
        const { aspects } = this.state;
        if (aspects.length) {
            return (
                <form className={'aspects-form'}>
                    {
                        aspects.map((aspect, aspectKey) => 
                            aspect.aspectConstraint.aspectMode === "SELECTION_ONLY"
                            ?
                                <select key={aspectKey} defaultValue={'0'}>
                                    <option disabled={true} value={'0'}>{aspect.localizedAspectName}</option>
                                    {
                                        aspect.aspectValues.map((aspectValue, aspectValueKey) => 
                                            <option 
                                                key={aspectValueKey} 
                                                value={aspectValue.localizedValue}
                                            >
                                                {aspectValue.localizedValue}
                                            </option>    
                                        )
                                    }
                                </select>
                            :
                                <input key={aspectKey} type="text" name={aspect.localizedAspectName} placeholder={aspect.localizedAspectName}></input>
                        )
                    }
                </form>
            )
        } else {
            return <Loader />;
        }
        
    }
}
