import React, { Component } from 'react'
import ChannelsForm from './ChannelsForm/index'
import CategoriesForm from './CategoriesForm/index'
import AspectsForm from './AspectsForm/index'
import './main.css';



export default class Content extends Component {
    constructor(props){
        super(props);
        this.state = {
          channel: '0',
          category: ''
        };
    }

    changeChannel = e => {
        this.setState({channel: e.target.value, category: ''});
    };

    aspectsNeeded = id => {
        this.setState({category: id});
    };

    render() {
        const { channel, category } = this.state;
        return (
            <div className="content-cont">
                <ChannelsForm channel={channel} changeChannel={this.changeChannel} />
                { channel !== '0' && <CategoriesForm channel={channel} aspectsNeeded={this.aspectsNeeded} /> }
                { category && <AspectsForm category={category} /> }
            </div>
        );
    }
}
