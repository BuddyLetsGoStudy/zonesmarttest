import React, { Component } from 'react';
import axios from 'axios';
import './main.css';
import PropTypes from 'prop-types';
import Loader from '../Loader/index'

export default class LoginForm extends Component {
    static propTypes = {
        loginSuccess: PropTypes.func
    }

    constructor(props){
        super(props);
        this.state = {
          email: '',
          password: '',
          isLoading: false,
          error: false
        };
    }

    onChange = e => this.setState({[e.target.name]: e.target.value});

    submit = e => {
        this.setState({isLoading: true});
        const { email, password } = this.state;
        e.preventDefault();
        const body = JSON.stringify({ email, password })
        const headers = {
          'Content-Type': 'application/json'
        }
        axios.post('https://zonesmart.su/api/auth/jwt/create/', body, {headers})
          .then(res => {
            this.setState({isLoading: false});
            const { refresh, access } = res.data;
            localStorage.setItem('refresh', refresh);
            localStorage.setItem('access', access);
            this.props.loginSuccess();
          })
          .catch(err => {
            this.setState({isLoading: false, error: true});
            console.log(err);
          })
    };

    render() {
        const { email, password, isLoading, error } = this.state;
        if (isLoading) {
            return <Loader />
        } else {
            return (
                <form className='authForm' onSubmit={this.submit}>
                    { error && <div className={'error'}>Incorrect credentials</div> }
                    <input type='text' name='email' value={email} onChange={this.onChange} placeholder={'E-mail'}></input>
                    <input type='password' name='password' value={password} onChange={this.onChange} placeholder={'Password'}></input>
                    <button>Proceed</button>
                </form>
            )
        }
        
    }
}
